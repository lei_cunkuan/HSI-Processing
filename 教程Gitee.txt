0.去git官网，下载gitbash管理工具，并安装
1. 切换至本地仓库，右键在此处打开gitbash
2. 初始化本地仓库：git init （创建相对应的配置文件夹），要上传到远程仓库的文件都由这里传出
3. 在“码云”上，新建仓库，可以选择对应的类型（公开、私有）以及描述和协议。创完后，点击“克隆/下载”负责链接地址。
4. 连接至远程仓库：git remote add origin https://gitee.com/lei_cunkuan/HSI-Processing.git 这里相当于知道往哪里上传/下载
5. 将指定的单分支远程库里面的全部文件下载下来：git pull origin master
6. 上传文件：git add .(上传所有文件)/ git add "pca_svm"(上传指定文件夹)
7. 为上传文件添加描述：git commit -m "Feature selection and classification for hyperspectral image"
8. 将本地仓库同步到远程仓库：git push origin master
9. 回到“码云”，刷新就可以看见
