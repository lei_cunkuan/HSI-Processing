function [ label_case ] = mark_case( label,rate )
%MARK_CASE 此处显示有关此函数的摘要
%   此处显示详细说明
    [rsp]=tabulate(label);
    rsp(rsp(:,1)==0,:)=[];
    if( length(rate)==1 && rate>0 && rate<1)
        label_case = ceil(rsp(:,2)*rate);
    else
        if (length(rate)==1 && rate>1)
            label_case = (rsp(:,2)>=rate)*rate+(rsp(:,2)<rate).*rsp(:,2);
        else
            if(length(rate)==size(rsp,1))
                label_case = rate;
            else
                fprintf('标记出错！！\n');
            end
        end
    end       
end

