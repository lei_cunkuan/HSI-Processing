function [ data, label ] = load_data(file_name,band_set)
%LOAD_DATA 此处显示有关此函数的摘要
%   此处显示详细说明
    addpath(genpath(cd));    
    switch(file_name)
        case 'Indian'
            load Indian_pines_corrected;
            load Indian_pines_gt;
            data = Indian_pines_corrected(:,:,band_set);
            label = Indian_pines_gt(:);
        case 'PaviaU'
            load PaviaU;
            load PaviaU_gt;
            data = PaviaU(:,:,band_set);
            label = PaviaU_gt(:);
        case 'Salinas'
            load Salinas_corrected;
            load Salinas_gt;
            data = Salinas_corrected(:,:,band_set);
            label = Salinas_gt(:);
        otherwise
            fprintf('No such file, select from Indian/PaviaU/Salinas');
    end
end

