close all;
clear;
clc;
addpath(genpath(cd));
File_Names={'Indian','PaviaU','Salinas'};
for data_i=1:3
    file_name=File_Names{data_i};
    cd('C:\Aplin_EPF&IFRF\');
    Method_name='IFRF';
    folder_name0=[file_name,'_report'];
    mkdir(folder_name0);
    cd(folder_name0);
    switch(file_name)
        case 'Indian'
            for rate =0.1:0.1:0.1
                clearvars -except data_i rate file_name File_Names Method_name;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10
                    close all;
                    tic;
                    [ data,label,label_case] = load_data(file_name,rate);
                    [Train_pos,Test_pos,Predict,OA,Kappa,AA] = hiden_for_IFRF(data,label,label_case);
                    Time_cost(iter_num)=toc;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    AA_report(:,iter_num)=AA;
                    Pred_pos(:,iter_num)=Test_pos;
                    Pred_mat(:,iter_num)=Predict;
                end
                save IFRF_Indian OA_Kappa AA_report Pred_pos Pred_mat Time_cost;
                cd ..;
            end
        case 'PaviaU'
            for rate =0.1:0.1:0.1
                clearvars -except data_i rate file_name File_Names;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10
                    tic;
                    close all;
                    [ data,label,label_case] = load_data(file_name,rate);
                    [Train_pos,Test_pos,Predict,OA,Kappa,AA] = hiden_for_IFRF(data,label,label_case);
                    Time_cost(iter_num)=toc;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    AA_report(:,iter_num)=AA;
                    Pred_pos(:,iter_num)=Test_pos;
                    Pred_mat(:,iter_num)=Predict;
                end
                save IFRF_PU OA_Kappa AA_report Pred_pos Pred_mat Time_cost;
                cd ..;
            end
        case 'Salinas'
            for rate =0.002:0.002:0.008
                clearvars -except data_i rate file_name File_Names;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10
                    tic;
                    [ data,label,label_case] = load_data(file_name,rate);
                    [Train_pos,Test_pos,Predict,OA,Kappa,AA] = hiden_for_IFRF(data,label,label_case);
                    Time_cost(iter_num)=toc;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    AA_report(:,iter_num)=AA;
                    Pred_pos(:,iter_num)=Test_pos;
                    Pred_mat(:,iter_num)=Predict;
                end
                save IFRF_Salinas OA_Kappa AA_report Pred_pos Pred_mat Time_cost;
                cd ..;
            end
        otherwise
            fprintf('No such file!\n');
    end
    cd ..;
end

