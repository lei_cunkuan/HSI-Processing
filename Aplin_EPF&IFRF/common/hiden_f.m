function [SVMresult] = hiden_f(Train_y,Train_x,Test_y,Test_x,data_2d,label_1d,label,Train_pos,Test_pos)
%HIDEN_F 此处显示有关此函数的摘要
%   此处显示详细说明
    [~,~,~,Predict,model] = svm_c(Train_y,Train_x,Test_y,Test_x);
    [Predict_back,~,~] = svmpredict(label_1d(label_1d==0),data_2d(label_1d==0,:),model);
    SVMresult=zeros(prod(size(label)),1);
    SVMresult(label_1d==0)=Predict_back;
    SVMresult(Train_pos)=Train_y;
    SVMresult(Test_pos)=Predict;
    SVMresult=reshape(SVMresult,size(label));
end

