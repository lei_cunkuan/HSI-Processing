function [Train_pos,Test_pos,Predict,OA,Kappa,AA] = hiden_for_IFRF(data,label,label_case)
%HIDEN_FOR_IFRF 此处显示有关此函数的摘要
%   此处显示详细说明
    [row, col, ~] = size(data);
    img2=average_fusion(data,20);
    %%% normalization
    layer=size(img2,3);
    fimg=reshape(img2,[row*col layer]);
    [fimg] = scale_new(fimg);
    fimg=reshape(fimg,[row col layer]);
    %%% IFRF feature construction
    fimg=spatial_feature(fimg,200,0.3);
    [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos,data_2d,label_1d] = split_samples(fimg,label,label_case);
    [~,~,~,Predict,model] = svm_c(Train_y,Train_x,Test_y,Test_x);
    [Predict_back,~,~] = svmpredict(label_1d(label_1d==0),data_2d(label_1d==0,:),model);
    SVMresult=zeros(prod(size(label)),1);
    SVMresult(label_1d==0)=Predict_back;
    SVMresult(Train_pos)=Train_y;
    SVMresult(Test_pos)=Predict;
    SVMresult=reshape(SVMresult,size(label));
    [OA,Kappa,AA]=evaluations(SVMresult(Test_pos),Test_y);
end

