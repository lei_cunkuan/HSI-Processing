close all;
clear;
clc;
addpath(genpath(cd));
PC_num=30;      % PCA中的主成分个数
File_Names={'Indian','PaviaU','Salinas'};
for data_i=1:3
    file_name=File_Names{data_i};
    cd('C:\Aplin_SuperPCA\');
    Method_name='SuperPCA';
    folder_name0=[file_name,'_report'];
    mkdir(folder_name0);
    cd(folder_name0);
    switch(file_name)
        case 'Indian'
            for rate =0.02:0.02:0.10
                Sp_num=200;     % ERS切割的超像素的个数Indian:515; PU:1616; Salinas:556
                clearvars -except data_i rate file_name File_Names Sp_num PC_num ;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10
                    close all;
                    tic
                    [ data,label,label_case] = load_data(file_name,rate);    % 在这个文件里面设置标记状态
                    seg_map = cubseg(data,Sp_num);  % 基于第一主成分切割
                    [data_preprocessed] = SuperPCA(data,PC_num,seg_map);    % 经过SuperPCA处理后的数据
                    [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(data_preprocessed,label,label_case);
                    [OA,Kappa,AA,Predict] = svm_c(Train_y,Train_x,Test_y,Test_x);
                    Time_cost(iter_num)=toc;
                    AA_report(:,iter_num)=AA;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    Pred_mat(:,iter_num)=Predict;
                    Pred_pos(:,iter_num)=Test_pos;
                end
                save SuperPCA_Indian OA_Kappa AA_report Time_cost Pred_mat Pred_pos;
                cd ..;
            end
            
        case 'PaviaU'
            for rate =0.02:0.02:0.10
                Sp_num=400;
                clearvars -except data_i rate file_name File_Names Sp_num PC_num ;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10
                    close all;
                    tic
                    [ data,label,label_case] = load_data(file_name,rate);    % 在这个文件里面设置标记状态
                    seg_map = cubseg(data,Sp_num);  % 基于第一主成分切割
                    [data_preprocessed] = SuperPCA(data,PC_num,seg_map);    % 经过SuperPCA处理后的数据
                    [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(data_preprocessed,label,label_case);
                    [OA,Kappa,AA,Predict] = svm_c(Train_y,Train_x,Test_y,Test_x);
                    Time_cost(iter_num)=toc;
                    AA_report(:,iter_num)=AA;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    Pred_mat(:,iter_num)=Predict;
                    Pred_pos(:,iter_num)=Test_pos;
                end
                save SuperPCA_PU OA_Kappa AA_report Time_cost Pred_mat Pred_pos;
                cd ..;
            end
        case 'Salinas'
            for rate =0.002:0.002:0.01
                Sp_num=300;
                clearvars -except data_i rate file_name File_Names Sp_num PC_num ;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10
                    close all;
                    tic
                    [ data,label,label_case] = load_data(file_name,rate);    % 在这个文件里面设置标记状态
                    seg_map = cubseg(data,Sp_num);  % 基于第一主成分切割
                    [data_preprocessed] = SuperPCA(data,PC_num,seg_map);    % 经过SuperPCA处理后的数据
                    [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(data_preprocessed,label,label_case);
                    [OA,Kappa,AA,Predict] = svm_c(Train_y,Train_x,Test_y,Test_x);
                    Time_cost(iter_num)=toc;
                    AA_report(:,iter_num)=AA;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    Pred_mat(:,iter_num)=Predict;
                    Pred_pos(:,iter_num)=Test_pos;
                end
                save SuperPCA_Salinas OA_Kappa AA_report Time_cost Pred_mat Pred_pos;
                cd ..;
            end
        otherwise
            fprintf('No such file!\n');
    end
    cd ..;
end

