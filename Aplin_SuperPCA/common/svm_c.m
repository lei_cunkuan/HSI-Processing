function [OA,Kappa,AA,Predict] =svm_c(Train_y,Train_x,Test_y,Test_x)
%SVM_CODE 分类
     %% 参数寻优
     [~,bestc,bestg]=SVMcgForClass(Train_y,Train_x);
     cmd=[' -c ',num2str(bestc),' -g ',num2str(bestg)];
     model=svmtrain(Train_y,Train_x,cmd);
     [Predict,~,~] = svmpredict(Test_y,Test_x,model);
     %数据可视化
     figure;
     plot(Test_y,'b*-')
     hold on;
     plot(Predict,'r+-');
     title('数据可视化');
     legend('原始数据','回归数据');
     [OA,Kappa,AA]=evaluations(Predict,Test_y);
end

