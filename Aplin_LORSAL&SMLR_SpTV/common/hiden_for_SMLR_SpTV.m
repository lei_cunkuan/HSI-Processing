function [LORSAL_OA_Kappa,LORSAL_AA_report,SMLR_SpTV_OA_Kappa,SMLR_SpTV_AA_report,t_SpATV,t_LORSAL,Pred_pos,Pred_mat_SMLR_SpTV,Pred_mat_LORSAL] = hiden_for_SMLR_SpTV( data,label,label_case,lambda,beta,Max_iter)
%HIDEN_FOR_SMLR_SPTV 此处显示有关此函数的摘要
%   此处显示详细说明
    cls_info=setdiff(unique(label),0);
    [row,col,layer]=size(data);
    k=length(cls_info);
    label_2d=[];
    for i=1:k
        tmp_pos=find(label==cls_info(i));
        tmp_res=[tmp_pos,label(tmp_pos)];
        label_2d=[label_2d;tmp_res];
    end
    sz = size(data);
    data = ToVector(data);
    data = data'; 
    sz(3) = size(data,1);
    % nomalize the image
    data = data./repmat(sqrt(sum(data.^2)),sz(3),1);
    label_2d = label_2d';
    %classification accuracy
    % start Monte Carlo runs
    fprintf('\nStarting Monte Carlo runs \n');
    for iter = 1:Max_iter
        fprintf('MC run %d \n', iter);
        tic;
        indexes = train_test_random_newvector(label_2d(2,:),label_case);   
        Train_Pos(:,iter)=indexes;
        train1 = label_2d(:,indexes);
        test1 = label_2d;
        test1(:,indexes) = [];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        train = data(:,train1(1,:));        
        test = data(:,test1(1,:));
        y = train1(2,:);        % train label 1*1043
        % reindex to natural indeces
        x = train;              % train data 200*1043
        % space dimension
        sigma = 0.8;
        [d,n] =size(x);
        % build |x_i-x_j| matrix
        nx = sum(x.^2);
        [X,Y] = meshgrid(nx);
        dist=X+Y-2*x'*x;
        scale = mean(dist(:));
        % build design matrix (kernel)
        K=exp(-dist/2/scale/sigma^2);
        % set first line to one
        K = [ones(1,n); K];
        % learn the regressors
        [w,L] = LORSAL(K,y,lambda,beta);
        p = splitimage2(data,x,w,scale,sigma);
        t_LORSAL(iter) = toc;
        % constrained least squares l2-TV (nonisotropic)
        lambda_TV = 2;
        Traning_logic_matrix = zeros(k,sum(label_case));
        for i = 1:sum(label_case)
            Traning_logic_matrix(y(i),i)=1;
        end
        Training_Info.Traning_logic_matrix=Traning_logic_matrix;
        Training_Info.index = train1(1,:);
        [p_tv,res] = SAL_SpTV_new(p,Training_Info,'MU',0.05,...
            'LAMBDA_TV', lambda_TV, 'TV_TYPE','niso',...
            'IM_SIZE',[row,col],'AL_ITERS',layer,  'VERBOSE','no');
        t_SpATV(iter) = toc;
        tic;
        %% classification results
        %-------------------------------------------
        %LORSAL classification
        [maxp,class] = max(p);
        %SMLR_SpTV classification
        [maxp,class1] = max(p_tv);
        %%%-- LORSAL classification accuracy
        OA_LORSAL_AL(iter)     =  sum(class(test1(1,:))==test1(2,:))/length(test1(2,:));
        [a.OA,a.kappa,a.AA,a.CA] = calcError( test1(2,:)-1, class(test1(1,:))-1, 1: k);
        LORSAL_OA_Kappa(iter,1) = a.OA;
        LORSAL_OA_Kappa(iter,2) = a.kappa;        
        LORSAL_AA_report(:,iter) = a.CA;
        LORSAL_Cls_Map(:,:,iter) = reshape(class,row,col);
        Test_pos=test1(1,:)';
        Pred_pos(:,iter)=Test_pos;
        Pred_mat_LORSAL(:,iter)=LORSAL_Cls_Map(Test_pos);
        % SMLR_SpTV classification accuracy
        OA_SMLR_SpTV(iter) = sum(class1(test1(1,:))==test1(2,:))/length(test1(2,:));
        [a.OA,a.kappa,a.AA,a.CA] = calcError( test1(2,:)-1, class1(test1(1,:))-1, 1: k);
        SMLR_SpTV_OA_Kappa(iter,1) = a.OA;
        SMLR_SpTV_OA_Kappa(iter,2) = a.kappa;
        SMLR_SpTV_AA_report(:,iter) = a.CA;
        SMLR_SpTV_Cls_Map(:,:,iter)=reshape(class1,row,col);
        Pred_mat_SMLR_SpTV(:,iter)=SMLR_SpTV_Cls_Map(Test_pos);
        
    end  
end

