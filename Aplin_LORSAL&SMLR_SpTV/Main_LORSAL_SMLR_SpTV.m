close all;
clear;
clc;
addpath(genpath(cd));
File_Names={'Indian','PaviaU','Salinas'};
for data_i=1:3
    file_name=File_Names{data_i};
    cd('C:\Aplin_LORSAL&SMLR_SpTV\');
    Method_name='LORSAL&SMLR_SpTV';
    folder_name0=[file_name,'_report'];
    mkdir(folder_name0);
    cd(folder_name0);
    switch(file_name)
        case 'Indian'
            for rate =0.02:0.02:0.08
                clearvars -except data_i rate file_name File_Names Method_name;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                [ data,label,label_case] = load_data(file_name,rate);
                [LORSAL_OA_Kappa,LORSAL_AA_report,SMLR_SpTV_OA_Kappa,SMLR_SpTV_AA_report,t_SpATV,t_LORSAL,Pred_pos,Pred_mat_SMLR_SpTV,Pred_mat_LORSAL] = hiden_for_SMLR_SpTV( data,label,label_case,lambda,beta,iter_num);
                [OA_Kappa,AA_report,Time_cost,Pred_pos,Pred_mat] = standard_store(LORSAL_OA_Kappa,LORSAL_AA_report,t_LORSAL,Pred_pos,Pred_mat_LORSAL);
                save LORSAL_Indian OA_Kappa AA_report Time_cost Pred_pos Pred_mat;
                clear OA_Kappa AA_report Time_cost Pred_mat;
                [OA_Kappa,AA_report,Time_cost,Pred_pos,Pred_mat] = standard_store(SMLR_SpTV_OA_Kappa,SMLR_SpTV_AA_report,t_SpATV,Pred_pos,Pred_mat_SMLR_SpTV);
                save SMLR_SpTV_Indian OA_Kappa AA_report Time_cost Pred_pos Pred_mat;
                cd ..;
            end
        case 'PaviaU'
            for rate =0.02:0.02:0.08
                clearvars -except data_i rate file_name File_Names;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                [ data,label,label_case] = load_data(file_name,rate);
                [LORSAL_OA_Kappa,LORSAL_AA_report,SMLR_SpTV_OA_Kappa,SMLR_SpTV_AA_report,t_SpATV,t_LORSAL,Pred_pos,Pred_mat_SMLR_SpTV,Pred_mat_LORSAL] = hiden_for_SMLR_SpTV( data,label,label_case,lambda,beta,iter_num);
                [OA_Kappa,AA_report,Time_cost,Pred_pos,Pred_mat] = standard_store(LORSAL_OA_Kappa,LORSAL_AA_report,t_LORSAL,Pred_pos,Pred_mat_LORSAL);
                save LORSAL_PU OA_Kappa AA_report Time_cost Pred_pos Pred_mat;
                clear OA_Kappa AA_report Time_cost Pred_mat;
                [OA_Kappa,AA_report,Time_cost,Pred_pos,Pred_mat] = standard_store(SMLR_SpTV_OA_Kappa,SMLR_SpTV_AA_report,t_SpATV,Pred_pos,Pred_mat_SMLR_SpTV);
                save SMLR_SpTV_PU OA_Kappa AA_report Time_cost Pred_pos Pred_mat;
                cd ..;
            end
        case 'Salinas'
            for rate =0.002:0.002:0.008
                clearvars -except data_i rate file_name File_Names;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                [ data,label,label_case] = load_data(file_name,rate);                
                [LORSAL_OA_Kappa,LORSAL_AA_report,SMLR_SpTV_OA_Kappa,SMLR_SpTV_AA_report,t_SpATV,t_LORSAL,Pred_pos,Pred_mat_SMLR_SpTV,Pred_mat_LORSAL] = hiden_for_SMLR_SpTV( data,label,label_case,lambda,beta,iter_num);
                [OA_Kappa,AA_report,Time_cost,Pred_pos,Pred_mat] = standard_store(LORSAL_OA_Kappa,LORSAL_AA_report,t_LORSAL,Pred_pos,Pred_mat_LORSAL);
                save LORSAL_Salinas OA_Kappa AA_report Time_cost Pred_pos Pred_mat;
                clear OA_Kappa AA_report Time_cost Pred_mat;
                [OA_Kappa,AA_report,Time_cost,Pred_pos,Pred_mat] = standard_store(SMLR_SpTV_OA_Kappa,SMLR_SpTV_AA_report,t_SpATV,Pred_pos,Pred_mat_SMLR_SpTV);
                save SMLR_SpTV_Salinas OA_Kappa AA_report Time_cost Pred_pos Pred_mat;
                cd ..;
            end
        otherwise
            fprintf('No such file!\n');
    end
    cd ..;
end

