close all;
clear;
clc;
addpath(genpath(cd));
PC_num=30;      % PCA中的主成分个数
Multi_scale_Sp_num=[300,500,700];     % ERS切割的超像素的个数Indian:515; PU:1616; Salinas:556
file_name='Indian';
Multi_Voting=[];
for iter_num=1:10   % 程序独立运行的次数
    tic
    for k=1:length(Multi_scale_Sp_num)
        Sp_num=Multi_scale_Sp_num(k);
        close all;
        tic
        [ data,label,label_case] = load_data(file_name);    % 在这个文件里面设置标记状态
        seg_map = cubseg(data,Sp_num);  % 基于第一主成分切割
        [data_preprocessed] = SuperPCA(data,PC_num,seg_map);    % 经过SuperPCA处理后的数据
        [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(data_preprocessed,label,label_case);
        [~,~,~,Multi_Voting(:,k)] = svm_c(Train_y,Train_x,Test_y,Test_x);        
    end
    for j=1:size(Multi_Voting,1)
        temp_pred=Multi_Voting(j,:);
        res=tabulate(temp_pred);
        tmp_pred=res(res(:,2)==max(res(:,2)),1);
        if length(tmp_pred)==1
            Final_pred(j,1)=tmp_pred;
        else
            Final_pred(j,1)=tmp_pred(1);
        end
    end
    [OA,Kappa,AA]=evaluations(Final_pred,Test_y);   % 以最后一次标记的准
    Time_cost(iter_num)=toc;
    AA_report(:,iter_num)=AA;
    OA_Kappa(iter_num,1)=OA;
    OA_Kappa(iter_num,2)=Kappa;
    Pred_mat(:,iter_num)=Final_pred;
    Pred_pos(:,iter_num)=Test_pos;
end
save MSuperPCA_result_Indian OA_Kappa AA_report Time_cost Pred_mat Pred_pos;