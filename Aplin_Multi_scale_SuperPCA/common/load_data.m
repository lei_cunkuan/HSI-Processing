function [ data,label,label_case] = load_data(file_name)
%LOAD_DATA 此处显示有关此函数的摘要
%   此处显示详细说明
    if strcmp(file_name,'Indian')
        load Indian_pines_corrected;        
        load Indian_pines_gt;
        data=Indian_pines_corrected;
        data=data/max(data(:));     % 简单的一种归一化
        label=Indian_pines_gt;
        label_case=[5,143,83,24,49,73,3,48,2,98,246,60,21,127,39,10];
    elseif strcmp(file_name,'Salinas')
        load Salinas_corrected;
        load Salinas_gt;
        data=Salinas_corrected;
        data=data/max(data(:));
        label=Salinas_gt;
        label_case=[21,38,20,14,27,40,36,113,63,33,11,20,10,11,73,19];
    elseif strcmp(file_name,'PaviaU')
        load PaviaU;
        load PaviaU_gt;
        data=PaviaU;
        data=data/max(data(:));
        label=PaviaU_gt;
        label_case=[664,1865,210,307,135,503,133,369,95];
    else
        fprintf('No such dataset!\n[Indian,PaviaU,Salinas which one do you want execute:]');
    end        
end

