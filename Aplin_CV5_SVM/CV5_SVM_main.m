close all;
clear;
clc;
addpath(genpath(cd));
File_Names={'Indian','PaviaU','Salinas'};
for data_i=1:3
    file_name=File_Names{data_i};    
    cd('C:\Aplin_CV5_SVM\');
    Method_name='CV5_SVM';
    folder_name0=[file_name,'_report'];
    mkdir(folder_name0);
    cd(folder_name0);
    switch(file_name)
        case 'Indian'
            for rate =0.02:0.02:0.08
                clearvars -except data_i rate file_name File_Names Method_name;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10   % 程序独立运行的次数
                    close all;
                    tic;
                    [ data,label,label_case] = load_data(file_name,rate);
                    [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(data,label,label_case);
                    [OA,Kappa,AA,Predict] =svm_c(Train_y,Train_x,Test_y,Test_x);
                    Time_cost(iter_num,1)=toc;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    AA_report(:,iter_num)=AA;
                    Pred_mat(:,iter_num)=Predict;
                    Pred_pos(:,iter_num)=Test_pos;
                end
                save CV5_SVM_Indian OA_Kappa AA_report Time_cost Pred_mat Pred_pos;
                cd ..;
            end
        case 'PaviaU'
            for rate =0.02:0.02:0.08
                clearvars -except data_i rate file_name File_Names;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10   % 程序独立运行的次数
                    close all;
                    tic;
                    [ data,label,label_case] = load_data(file_name,rate);
                    [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(data,label,label_case);
                    [OA,Kappa,AA,Predict] =svm_c(Train_y,Train_x,Test_y,Test_x);
                    Time_cost(iter_num,1)=toc;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    AA_report(:,iter_num)=AA;
                    Pred_mat(:,iter_num)=Predict;
                    Pred_pos(:,iter_num)=Test_pos;
                end
                save CV5_SVM_PU OA_Kappa AA_report Time_cost Pred_mat Pred_pos;
                cd ..;
            end
        case 'Salinas'
            for rate =0.002:0.002:0.008
                clearvars -except data_i rate file_name File_Names;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10   % 程序独立运行的次数
                    close all;
                    tic;
                    [ data,label,label_case] = load_data(file_name,rate);
                    [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(data,label,label_case);
                    [OA,Kappa,AA,Predict] =svm_c(Train_y,Train_x,Test_y,Test_x);
                    Time_cost(iter_num,1)=toc;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    AA_report(:,iter_num)=AA;
                    Pred_mat(:,iter_num)=Predict;
                    Pred_pos(:,iter_num)=Test_pos;
                end
                save CV5_SVM_Salinas OA_Kappa AA_report Time_cost Pred_mat Pred_pos;
                cd ..;
            end
        otherwise
            fprintf('No such file!\n');
    end
    cd ..;
end

