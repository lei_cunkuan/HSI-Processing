close all;
clear;
clc;
addpath(genpath(cd));
File_Names={'Indian','PaviaU','Salinas'};
for data_i=1:3
    in_param.nfold = 5;
    file_name=File_Names{data_i};
    cd('C:\Aplin_SC-MK\');
    Method_name='SC-MK';
    folder_name0=[file_name,'_report'];
    mkdir(folder_name0);
    cd(folder_name0);
    switch(file_name)
        case 'Indian'
            for rate =0.02:0.02:0.08
                Sp_num=515;
                Pc_num=3;
                clearvars -except data_i rate file_name File_Names Pc_num Sp_num in_param;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10
                    tic;
                    [ data,label,label_case] = load_data(file_name,rate);
                    [mean_matix,weighted_matrix,train_img,test_SL,row,col] = trans_data(data,label,Sp_num,Pc_num,label_case);
                    [class_label, out_param] = multiple_kernels_svm(data,mean_matix,weighted_matrix,train_img,in_param);
                    test_y=test_SL(2,:);
                    predict=class_label(test_SL(1,:))';
                    [OA,Kappa,AA]=evaluations(predict,test_y);
                    class_map=reshape(class_label, row,col);
                    resultmap=class_map.*(label~=0);
                    Time_cost(iter_num)=toc;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    AA_report(:,iter_num)=AA;
                    Pred_pos(:,iter_num)=test_SL(1,:)';
                    Pred_mat(:,iter_num)=predict';
                end
                save SC-MK_Indian OA_Kappa AA_report Time_cost Pred_mat Pred_pos;
                cd ..;
            end
        case 'PaviaU'
            for rate =0.02:0.02:0.08
                Sp_num=1616;
                Pc_num=3;
                clearvars -except data_i rate file_name File_Names Pc_num Sp_num in_param;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10
                    tic;
                    [ data,label,label_case] = load_data(file_name,rate);
                    [mean_matix,weighted_matrix,train_img,test_SL,row,col] = trans_data(data,label,Sp_num,Pc_num,label_case);
                    [class_label, out_param] = multiple_kernels_svm(data,mean_matix,weighted_matrix,train_img,in_param);
                    test_y=test_SL(2,:);
                    predict=class_label(test_SL(1,:))';
                    [OA,Kappa,AA]=evaluations(predict,test_y);
                    class_map=reshape(class_label, row,col);
                    resultmap=class_map.*(label~=0);
                    Time_cost(iter_num)=toc;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    AA_report(:,iter_num)=AA;
                    Pred_pos(:,iter_num)=test_SL(1,:)';
                    Pred_mat(:,iter_num)=predict';
                end
                save SC-MK_PU OA_Kappa AA_report Time_cost Pred_mat Pred_pos;
                cd ..;
            end
        case 'Salinas'
            for rate =0.002:0.002:0.008
                Sp_num=556;
                Pc_num=3;
                clearvars -except data_i rate file_name File_Names Pc_num Sp_num in_param;
                folder_name1=[file_name,'_rate=',num2str(rate)];
                mkdir(folder_name1);
                cd(folder_name1);
                for iter_num=1:10
                    tic;
                    [ data,label,label_case] = load_data(file_name,rate);
                    [mean_matix,weighted_matrix,train_img,test_SL,row,col] = trans_data(data,label,Sp_num,Pc_num,label_case);
                    [class_label, out_param] = multiple_kernels_svm(data,mean_matix,weighted_matrix,train_img,in_param);
                    test_y=test_SL(2,:);
                    predict=class_label(test_SL(1,:))';
                    [OA,Kappa,AA]=evaluations(predict,test_y);
                    class_map=reshape(class_label, row,col);
                    resultmap=class_map.*(label~=0);
                    Time_cost(iter_num)=toc;
                    OA_Kappa(iter_num,1)=OA;
                    OA_Kappa(iter_num,2)=Kappa;
                    AA_report(:,iter_num)=AA;
                    Pred_pos(:,iter_num)=test_SL(1,:)';
                    Pred_mat(:,iter_num)=predict';
                end                
                save SC-MK_Salinas OA_Kappa AA_report Time_cost Pred_mat Pred_pos;
                cd ..;
            end
        otherwise
            fprintf('No such file!\n');
    end
    cd ..;
end
