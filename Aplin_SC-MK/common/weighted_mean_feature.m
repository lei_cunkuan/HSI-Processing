 function [ weigthed_matrix ] = weighted_mean_feature( labels, img, sup_img)
%=================================================================================
%This function is used to extract spatial information among superpixels
%input arguments:  labels          : superpixel segmentation map
%                  img             : dimension-reduced HSI
%                  sup_img         : superpixels image                  
%output arguments: weigthed_matrix : feature matrix among each superpixels 
%=================================================================================
    MaxSegments=max(labels(:));
    [row, col, layer]=size(img);
    s=[row col];
    labels=double(labels);
    alfa=2;
    beta=0.7;
    sup_img=sup_img';
    for i=0:MaxSegments
        supind=find(labels==i);
        [M,N]=size(supind);
        if M<1
            continue;
        end
        mainclass=labels(supind);
        [a,b]=ind2sub(s,supind);    
        centebrseed=[floor(mean(a)),floor(mean(b))]; 
        n1=diag(labels(max(1,a-1),b));
        n2=diag(labels(min(row,a+1),b));
        n3=diag(labels(a,max(1,b-1)));
        n4=diag(labels(a,min(col,b+1)));
        a=unique([n1;n2;n3;n4]);    
        a(a==i)=[];
        meanv=sup_img(a(:)+1,:); 
        meanv=mean(meanv);
        centerv=sup_img(i+1,:);
        meanv=meanv*alfa;   
        newcenter=(centerv*beta+meanv)/2;
        for j=1:M
            weigthed_matrix(:,supind(j))=newcenter;
        end
    end
    weigthed_matrix=weigthed_matrix';
    weigthed_matrix=reshape(weigthed_matrix,row, col, layer);
end
