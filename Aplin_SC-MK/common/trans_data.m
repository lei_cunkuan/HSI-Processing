function [mean_matix,weighted_matrix,train_img,test_SL,row,col] = trans_data(data,label,Sp_num,Pc_num,label_case)
%TRANS_DATA 此处显示有关此函数的摘要
%   此处显示详细说明
    no_classes=length(setdiff(unique(label),0));
    seg_map = cubseg(data,Sp_num);
    [mean_matix,super_img,~] = mean_feature (data,seg_map);
    [ weighted_matrix ] = weighted_mean_feature( seg_map,data,super_img);

    [row,col,layer]=size(data);
    data2d=reshape(data,row*col,layer);
    x=compute_mapping(data2d,'PCA',Pc_num);
    img1=reshape(x, row,col, Pc_num);
    img1=mat2gray(img1);
    img1=im2uint8(img1);

    [ row,col,layer] = size(img1);
    img1=reshape(img1,row*col,layer)';
    RandSampled_Num=label_case;
    Nonzero_map = zeros(row,col);
    Nonzero_index =  find(label ~= 0);
    Nonzero_map(Nonzero_index)=1;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Train_Label = [];
    Train_index = [];
    for ii = 1: no_classes
       index_ii =  find(label == ii);
       class_ii = ones(length(index_ii),1)* ii;
       Train_Label = [Train_Label class_ii'];
       Train_index = [Train_index index_ii'];   
    end
    trainall = zeros(2,length(Train_index));
    trainall(1,:) = Train_index;
    trainall(2,:) = Train_Label;

    %% Create the Training set with randomly sampling 3-D Dataset and its correponding index
    indexes =[];
    for i = 1: no_classes
      W_Class_Index = find(Train_Label == i);
      Random_num = randperm(length(W_Class_Index));
      Random_Index = W_Class_Index(Random_num);
      Tr_Index = Random_Index(1:RandSampled_Num(i));
      indexes = [indexes Tr_Index];
    end   
    indexes = indexes';
    train_SL = trainall(:,indexes);
    train_samples = img1(:,train_SL(1,:))';
    train_labels= train_SL(2,:)';
    %% Create the Testing set with randomly sampling 3-D Dataset and its correponding index
    test_SL = trainall;
    test_SL(:,indexes) = [];
    test_samples = img1(:,test_SL(1,:))';
    test_labels = test_SL(2,:)';
    %% Generate spectral feature
    train_img=zeros(row,col);          
    train_img(train_SL(1,:))=train_SL(2,:);  
end

