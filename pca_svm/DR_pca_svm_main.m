close all;
clear;
clc;
addpath(genpath(cd));
file_name='Salinas'; % Select one dataset from 'Indian,PaviaU,Salinas'.
[data,label] = load_data(file_name);
[~, SCORE, ~] = pca(data);
for K=30:5:30
    ext_data=SCORE(:,1:K);
    [ min_max_std_one,mean_std_std_one] = normalization( ext_data );    
    for rate=0.01
        for time =1:10
            tic;
            [ label_case ] = mark_case( label,rate);
            [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(min_max_std_one,label,label_case);
            [OA,Kappa,AA,Predict,model] = svm_c(Train_y,Train_x,Test_y,Test_x);
            Time_cost(time)=toc;
            OA_Kappa(time,1)=OA;
            OA_Kappa(time,2)=Kappa;
            AA_report(:,time)=AA;            
            Pred_pos(:,time)=Test_pos;
            Pred_mat(:,time)=Predict;
        end    
    end
    cd('C:\pca_svm\report');
    folder_name=[file_name,'_K=',num2str(K),'_rate=',num2str(rate)];
    mkdir(folder_name);
    cd(folder_name);
    save Report OA_Kappa AA_report Time_cost Pred_pos Pred_mat label_case;
end
    
