function [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(data,label,label_case)
%SPLIT_SAMPLES 此处显示有关此函数的摘要
%   此处显示详细说明
    cls_info=setdiff(unique(label),0);
    Train_x=[];
    Train_y=[];
    Test_x=[];
    Test_y=[];
    Train_pos=[];
    Test_pos=[];
    for i=1:length(cls_info)
        temp_pos=find(label==cls_info(i));
        temp_pos=temp_pos(randperm(length(temp_pos)));
        temp_data=data(temp_pos,:);
        temp_label=label(temp_pos,:);
        if(label_case(i)>length(temp_pos))
            fprintf('超出样本空间,程序将在5秒后关闭!\n');
            pause(5);
            exit();
        else
            Train_x=[Train_x;temp_data(1:label_case(i),:)];
            Train_y=[Train_y;temp_label(1:label_case(i),:)];
            Train_pos=[Train_pos;temp_pos(1:label_case(i),1)];
            Test_x=[Test_x;temp_data((label_case(i)+1):end,:)];
            Test_y=[Test_y;temp_label((label_case(i)+1):end,:)];
            Test_pos=[Test_pos;temp_pos((label_case(i)+1):end,1)];
        end
    end
end

