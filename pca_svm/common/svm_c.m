function [OA,Kappa,AA,Predict,model] =svm_c(Train_y,Train_x,Test_y,Test_x)
%SVM_CODE 分类
     %% 参数寻优
     [~,bestc,bestg]=SVMcgForClass(Train_y,Train_x);
     cmd=[' -c ',num2str(bestc),' -g ',num2str(bestg)];
     model=svmtrain(Train_y,Train_x,cmd);
     [Predict,~,~] = svmpredict(Test_y,Test_x,model);
     [OA,Kappa,AA]=evaluations(Predict,Test_y);
     fprintf('OA= %f, Kappa= %f\n',OA,Kappa);
end

