function [ min_max_std_one,mean_std_std_one] = normalization( data )
%STD_ONE 此处显示有关此函数的摘要
%   此处显示详细说明
    [~,  col]=size(data);
    min_max_std_one=zeros(size(data));
    mean_std_std_one=zeros(size(data));
    for i=1:col
        Max_value=max(data(:,i));
        Min_value=min(data(:,i));
        min_max_std_one(:,i)=(data(:,i)-Min_value)/(Max_value-Min_value);
        Mean_value=mean(data(:,i));
        Std_value=std(data(:,i));
        mean_std_std_one(:,i)=(data(:,i)-Mean_value)/Std_value;
    end
end

 