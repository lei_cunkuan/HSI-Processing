close all;
clear;
clc;
addpath(genpath(cd));
file_name='Indian'; % Select one dataset from 'Indian,PaviaU,Salinas'.
[data,label] = load_data(file_name);
[ min_max_std_one,mean_std_std_one] = normalization( data );
para = mean(var(min_max_std_one,0,2));
[kpca_data] = kernel_pca(min_max_std_one',30,'gauss',sqrt(para));
for K=30:5:30   
    for rate=0.1
        for time =1:10
            tic;
            [ label_case ] = mark_case( label,rate);
            [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(kpca_data,label,label_case);
            [OA,Kappa,AA,Predict,model] = svm_c(Train_y,Train_x,Test_y,Test_x);
            Time_cost(time)=toc;
            OA_Kappa(time,1)=OA;
            OA_Kappa(time,2)=Kappa;
            AA_report(:,time)=AA;            
            Pred_pos(:,time)=Test_pos;
            Pred_mat(:,time)=Predict;
        end    
    end
    cd('C:\kernel_pca_svm\report');
    folder_name=[file_name,'_K=',num2str(K),'_rate=',num2str(rate)];
    mkdir(folder_name);
    cd(folder_name);
    save Report OA_Kappa AA_report Time_cost Pred_pos Pred_mat label_case;
end
    
