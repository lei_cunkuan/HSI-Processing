function [bestacc,bestc,bestg] = SVMcgForClass(train_y,train_x,cmin,cmax,gmin,gmax,v,cstep,gstep,accstep)
%% about the parameters of SVMcg 
if nargin < 10 % nargin 表示函数的输入变量个数
    accstep = 1.5;
end
if nargin < 8
    accstep = 1.5;
    cstep = 1;
    gstep = 1;
end
if nargin < 7
    v = 5;
end
if nargin < 5
    accstep = 1.5;
    cstep = 1;
    gstep = 1;
    v = 5;
    gmax = 5;
    gmin = -5;
end
if nargin < 3
    accstep = 1.5;
    cstep = 1;
    gstep = 1;
    v = 5;
    gmax = 5;
    gmin = -5;
    cmax = 5;
    cmin = -5;
end
%% X:c Y:g cg:CVaccuracy
[X,Y] = meshgrid(cmin:cstep:cmax,gmin:gstep:gmax);
[m,n] = size(X);
cg = zeros(m,n);
eps = 10^(-4);

%% record acc with different c & g,and find the bestacc with the smallest c
bestc = 1;
bestg = 0.1;
bestacc = 0;
basenum = 2;
for i = 1:m
    for j = 1:n
        cmd = ['-v ',num2str(v),' -c ',num2str( basenum^X(i,j) ),' -g ',num2str( basenum^Y(i,j) )];
        cg(i,j) = svmtrain(train_y, train_x, cmd);
        
        if cg(i,j) <= 55
            continue;
        end
        
        if cg(i,j) > bestacc
            bestacc = cg(i,j);
            bestc = basenum^X(i,j);
            bestg = basenum^Y(i,j);
        end
        
        if abs( cg(i,j)-bestacc )<=eps && bestc > basenum^X(i,j)
            bestacc = cg(i,j);
            bestc = basenum^X(i,j);
            bestg = basenum^Y(i,j);
        end
        
    end
end