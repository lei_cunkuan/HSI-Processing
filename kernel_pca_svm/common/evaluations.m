function [OA,Kappa,AA]=evaluations(predict,test_y)
    Confusion_matrix = confusionmat(predict,test_y);%计算混淆矩阵
    OA = sum(diag(Confusion_matrix))/sum(sum(Confusion_matrix));%总体精度
    rowsum = sum(Confusion_matrix,2);
    colsum = sum(Confusion_matrix,1);
    N = sum(sum(Confusion_matrix));
    Kappa = (N*sum(diag(Confusion_matrix)) - colsum*rowsum)/(N^2-colsum*rowsum);%Kappa系数
    cls_info=setdiff(unique(test_y),0);
    correct=test_y-predict;
    for i=1:length(cls_info)
        temp=correct(test_y==cls_info(i))==0;
        AA(i,1)=sum(temp)/length(temp);
    end
    OA=roundn(100*OA,-2);
    Kappa=roundn(Kappa,-4);
    AA=roundn(100*AA,-2);
end