function [ data,label] = load_data(file_name)
%LOAD_DATA 此处显示有关此函数的摘要
%   此处显示详细说明
    if strcmp(file_name,'Indian')
        load Indian_pines_corrected;        
        load Indian_pines_gt;
        data=Indian_pines_corrected;
        [row,col,layer] = size(data);
        data=reshape(data,row*col,layer);        
        label=Indian_pines_gt;      
        label=label(:);
    elseif strcmp(file_name,'Salinas')
        load Salinas_corrected;
        load Salinas_gt;
        data=Salinas_corrected;  
        [row,col,layer] = size(data);
        data=reshape(data,row*col,layer); 
        label=Salinas_gt;    
        label=label(:);
    elseif strcmp(file_name,'PaviaU')
        load PaviaU;
        load PaviaU_gt;
        data=PaviaU;
        [row,col,layer] = size(data);
        data=reshape(data,row*col,layer);
        label=PaviaU_gt; 
        label=label(:);
    else
        fprintf('No such dataset!\n[Indian,PaviaU,Salinas which one do you want execute:]');
    end        
end

