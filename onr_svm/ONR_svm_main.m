close all;
clear;
clc;
addpath(genpath(cd));
file_name='Indian';
Rate=0.1;
Sel_num=30;
load Indian_pines_corrected;
tic
[ band_set ] = onr_api( Indian_pines_corrected,Sel_num);
Time_cost=toc;
[ data, label ] = load_data(file_name,band_set);
for time=1:1  
    [ label_case ] = mark_case( label,Rate );
    [Train_x,Train_y,Test_x,Test_y,Train_pos,Test_pos] = split_samples(data,label,label_case);
    [OA,Kappa,AA,Predict,model] = svm_c(Train_y,Train_x,Test_y,Test_x);
    Time_cost(time)=toc;
    OA_Kappa(time,1)=OA;
    OA_Kappa(time,2)=Kappa;
    AA_report(:,time)=AA;
    Pred_pos(:,time)=Test_pos;
    Pred_mat(:,time)=Predict;
end
cd('C:\onr_svm\report\');
folder_name=['ONR_',file_name,'_report'];
mkdir(folder_name);
cd(folder_name);
save Report OA_Kappa AA_report Time_cost Pred_pos Pred_mat label_case Time_cost;