function [ band_set ] = onr_api( data,Sel_num)
%ONR.API 此处显示有关此函数的摘要
%   此处显示详细说明
    X = permute(data, [3, 1, 2]);
%% normalization
    X = X(:, :);
    minv = min(X(:)); maxv = max(X(:));
    X = (X - minv) / (maxv - minv);
    %% run
%     k = 30; % Number of bands
    ONR_L = ONR_init(X');
    band_set = ONR(X, ONR_L, Sel_num);
end

